A small app I created for a test.

Requirements are app app that using a web service, tells you the weather in any place on the map and paints the map’s background according to the current temperature in that location.

Notes:

-I used a Storyboard but it's not needed. Working with branches and workmates probably this is not a good idea.

-It follows the typical MVC approach.

-Usually I add AFNetworking or RestKit (through Cocoapods) for networking, but as this example was easy, I used NSURLSession directly.

-It has tests using XCTest and OCMock.

-In the data model I added the networking and data mapping. This could be separated, or not. This is a small app.

-More notes in different comments in the code.

-The app requires iOS 8 and has been created using Xcode 6.3.2. It has 2 pods, so you should call 'pod update' and open the workspace file.