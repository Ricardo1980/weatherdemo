//
//  WeatherClientAPI.h
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Weather;
@class CLLocation;

@interface WeatherClientAPI : NSObject
-(void) weatherInlocation:(CLLocation*)location completion:(void(^)(Weather* weather, NSError *error))block;
@end
