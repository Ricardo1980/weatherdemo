//
//  WeatherClientAPI.m
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "WeatherClientAPI.h"
#import "Weather.h"
#import "MathUtil.h"
@import CoreLocation;

@implementation WeatherClientAPI

/*
 Gets current weather from a specific location, maps it to a Weather object and returns the block response in the main thread.
 http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139
 */
-(void) weatherInlocation:(CLLocation*)location completion:(void(^)(Weather* weather, NSError *error))block {
    
    NSAssert(block, @"There must be a block");
    
    if (!location) {
        NSError* error=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Location needed."}];
        if (block) block(nil, error);
        return;
    }

    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"Accept": @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    
    NSString* urlString=[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f", location.coordinate.latitude, location.coordinate.longitude];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        // test application level error
        if (error) {
            NSString* m=[NSString stringWithFormat:@"Error in the network. %@", error.localizedDescription];
            NSError* e=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:m}];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) block(nil, e);
            });
            return;
        }
        
        // test status code error
        // 2xx right
        NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
        if (HTTPResponse.statusCode/100!=2) {
            NSString* m=[NSString stringWithFormat:@"Error in the network request. Status code is %ld", (long)HTTPResponse.statusCode];
            NSError* e=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:m}];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) block(nil, e);
            });
            return;
        }
        
        // process json
        NSError *JSONError;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&JSONError];
        if (JSONError) {
            NSString* m=[NSString stringWithFormat:@"Error processing the JSON response. %@", JSONError.localizedDescription];
            NSError* e=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:m}];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) block(nil, e);
            });
            return;
        }
        
        // data mapping
        Weather* weather=[Weather new];
        weather.place=responseDict[@"name"];
        weather.countryCode=responseDict[@"sys"][@"country"];
        weather.countryCode=[weather.countryCode lowercaseString];
        weather.humidity=responseDict[@"main"][@"humidity"];
        
        // degress are in Kelvin so they must be converted to Celsius
        NSString* mainTemp=responseDict[@"main"][@"temp"];
        if (mainTemp) {
            weather.currentTemperature=@([MathUtil convertDegressKelvinToCelsius:mainTemp.floatValue]);
        }
        NSString* maxTemp=responseDict[@"main"][@"temp_max"];
        if (maxTemp) {
            weather.maxTemperature=@([MathUtil convertDegressKelvinToCelsius:maxTemp.floatValue]);
        }
        NSString* minTemp=responseDict[@"main"][@"temp_min"];
        if (minTemp) {
            weather.minTemperature=@([MathUtil convertDegressKelvinToCelsius:minTemp.floatValue]);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (block) block(weather, nil);
        });
    }];
    [dataTask resume];
}

@end