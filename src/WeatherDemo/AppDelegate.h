//
//  AppDelegate.h
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end

