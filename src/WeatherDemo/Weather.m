//
//  Weather.m
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "Weather.h"

@implementation Weather

/*
 Convert the current temperature to one color (hot=red, cold=blue or ultracold=white)
 */
-(UIColor*) currentTemperatureColor {
    
    if (!self.currentTemperature) return nil;
    
    // more than 50, just total red
    if (self.currentTemperature.floatValue>50) {
        return [UIColor redColor];
        
        // more than 17 and less than 50, red variation
    } else if (self.currentTemperature.floatValue>17 && self.currentTemperature.floatValue<=50) {
        float r=self.currentTemperature.floatValue/(50-17);
        return [UIColor colorWithRed:1 green:0.5-r/2 blue:0.5-r/2 alpha:1];
        
        // more than -20 and less than or equal to 17, just a blue variation
    } else if (self.currentTemperature.floatValue>-20 && self.currentTemperature.floatValue<=17) {
        float r=self.currentTemperature.floatValue/(17+20);
        return [UIColor colorWithRed:1-r green:1-r blue:1 alpha:1];
        
    }
    // less or equal than -20, north pole, just white like snow
    return [UIColor whiteColor];
}

-(NSString*) countryName {
    if (self.countryCode) {
        return [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:self.countryCode];
    }
    return nil;
}

@end
