//
//  main.m
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
