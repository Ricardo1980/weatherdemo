//
//  Weather.h
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface Weather : NSObject
@property (nonatomic, strong) NSString* place;
@property (nonatomic, strong) NSString* countryCode; // ISO
@property (nonatomic, strong) NSNumber* humidity; // 0 to 100, it's %
@property (nonatomic, strong) NSString* weatherDescription;
@property (nonatomic, strong) NSNumber* currentTemperature; // temperature values are in Celsius degrees
@property (nonatomic, strong) NSNumber* maxTemperature;
@property (nonatomic, strong) NSNumber* minTemperature;
@property (nonatomic, readonly) UIColor* currentTemperatureColor;
@property (nonatomic, readonly) NSString* countryName;
@end