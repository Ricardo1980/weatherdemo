//
//  MathUtil.h
//  WeatherDemo
//
//  Created by Ricardo Ruiz on 29/8/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MathUtil : NSObject
+(float) convertDegressKelvinToCelsius:(float)degreesKelvin;
@end
