//
//  MathUtil.m
//  WeatherDemo
//
//  Created by Ricardo Ruiz on 29/8/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "MathUtil.h"

@implementation MathUtil

// -273.15 is the minimum temperature in the universe
+(float) convertDegressKelvinToCelsius:(float)degreesKelvin {
    return MAX(degreesKelvin-273.15, -273.15);
}

@end
