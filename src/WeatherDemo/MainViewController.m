//
//  MainViewController.m
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "MainViewController.h"
#import "Weather.h"
#import "WeatherClientAPI.h"
#import <MBProgressHUD/MBProgressHUD.h>
@import MapKit;

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.weatherClientAPI=[WeatherClientAPI new];
    //[self getWeatherAndUpdateUI];
}

- (IBAction)onUpdateButtonPressed:(UIButton *)sender {
    [self getWeatherAndUpdateUI];
}

-(void) getWeatherAndUpdateUI {
    // ask for data to the data model using the center of the map (that will send a network request)
    CLLocation* centerLocation=[[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    self.updateButton.enabled=NO;
    [self.weatherClientAPI weatherInlocation:centerLocation completion:^(Weather *weather, NSError *error) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        self.updateButton.enabled=YES;
        if (error) {
            self.currentWeatherLabel.text=@"Error retrieving weather.";
            UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        } else {
            [self updateUIWithWeather:weather];
        }
    }];
}

-(void) updateUIWithWeather:(Weather*)weather {
    NSMutableArray* tokens=[NSMutableArray new];
    if (weather.place) {
        [tokens addObject:[NSString stringWithFormat:@"Place: %@", weather.place]];
    }
    if (weather.countryName) {
        [tokens addObject:[NSString stringWithFormat:@"Country: %@", weather.countryName]];
    }
    if (weather.humidity) {
        [tokens addObject:[NSString stringWithFormat:@"Humidity: %@ percentage", weather.humidity]];
    }
    if (weather.currentTemperature) {
        [tokens addObject:[NSString stringWithFormat:@"Current Temp: %@ Celsius", weather.currentTemperature]];
        self.view.backgroundColor=weather.currentTemperatureColor;
    } else {
        self.view.backgroundColor=nil;
    }
    if (weather.maxTemperature) {
        [tokens addObject:[NSString stringWithFormat:@"Max Temp: %@ Celsius", weather.maxTemperature]];
    }
    if (weather.minTemperature) {
        [tokens addObject:[NSString stringWithFormat:@"Min Temp: %@ Celsius", weather.minTemperature]];
    }
    self.currentWeatherLabel.text=[tokens componentsJoinedByString:@"\n"];
}

@end