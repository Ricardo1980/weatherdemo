//
//  MainViewController.h
//  WeatherDemo
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MKMapView;
@class WeatherClientAPI;

@interface MainViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *currentWeatherLabel;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (strong, nonatomic) WeatherClientAPI *weatherClientAPI;
@end
