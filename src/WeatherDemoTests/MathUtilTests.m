//
//  MathUtilTests.m
//  WeatherDemo
//
//  Created by Ricardo Ruiz on 29/8/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MathUtil.h"

@interface MathUtilTests : XCTestCase

@end

@implementation MathUtilTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testZeroKelvinDegress {
    XCTAssertEqualWithAccuracy ([MathUtil convertDegressKelvinToCelsius:0], -273.15, 0.01, @"0 kelvin degress must be equal to -273.15 celsius degrees.");
}

- (void)testLessThanZeroKelvinDegress {
    XCTAssertEqualWithAccuracy ([MathUtil convertDegressKelvinToCelsius:-10], -273.15, 0.01, @"-10 kelvin degress must be equal to -273.15 celsius degrees because less is impossible.");
}

- (void)test273Dot15KelvinDegress {
    XCTAssertEqualWithAccuracy ([MathUtil convertDegressKelvinToCelsius:273.15], 0, 0.01, @"273.15 kelvin degress must be equal to 0 celsius degrees.");
}

- (void)test300KelvinDegress {
    XCTAssertEqualWithAccuracy ([MathUtil convertDegressKelvinToCelsius:300], 26.85, 0.01, @"300 kelvin degress must be equal to 26.85 celsius degrees.");
}

@end


