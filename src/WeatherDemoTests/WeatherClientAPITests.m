//
//  WeatherClientAPITests.m
//  WeatherDemo
//
//  Created by Ricardo Ruiz on 29/8/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "WeatherClientAPI.h"
#import "Weather.h"
@import CoreLocation;

static const NSTimeInterval kRequestTimeout=15;

@interface WeatherClientAPITests : XCTestCase

@end

@implementation WeatherClientAPITests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testNetworkIntegration {
    XCTestExpectation* expectation = [self expectationWithDescription:@"Network request sent"];
    CLLocation* madridLocation=[[CLLocation alloc] initWithLatitude:40.428977 longitude:-3.684995];
    WeatherClientAPI* weatherClientAPI=[WeatherClientAPI new];
    [weatherClientAPI weatherInlocation:madridLocation completion:^(Weather* weather, NSError *error) {
        [expectation fulfill];
        XCTAssertNotNil(weather, @"There must be a weather object.");
        XCTAssertNil(error, @"There must not be an error object.");
    }];
    [self waitForExpectationsWithTimeout:kRequestTimeout handler:nil];
}

- (void)testNilLocation {
    XCTestExpectation* expectation = [self expectationWithDescription:@"Network request sent"];
    WeatherClientAPI* weatherClientAPI=[WeatherClientAPI new];
    [weatherClientAPI weatherInlocation:nil completion:^(Weather* weather, NSError* error) {
        [expectation fulfill];
        XCTAssertNil(weather, @"There must not be a weather object.");
        XCTAssertNotNil(error, @"There must be an error object.");
    }];
    [self waitForExpectationsWithTimeout:kRequestTimeout handler:nil];
}

@end
