//
//  WeatherDemoTests.m
//  WeatherDemoTests
//
//  Created by Ricardo on 9/7/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Weather.h"
#import <OCMock/OCMock.h>

@interface WeatherTests : XCTestCase

@end

@implementation WeatherTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testTooHotGivesRedColor {
    Weather* weather=[Weather new];
    weather.currentTemperature=@51;
    XCTAssertEqualObjects(weather.currentTemperatureColor, [UIColor redColor], @"More the 50 degress must have red color");
}

- (void)testTooColdGivesWhiteColor {
    Weather* weather=[Weather new];
    weather.currentTemperature=@(-21);
    XCTAssertEqualObjects(weather.currentTemperatureColor, [UIColor whiteColor], @"Less than 20 degress must have white color");
}

- (void)testThereIsColorWhenTemparatureAvailable {
    Weather* weather=[Weather new];
    weather.currentTemperature=@31;
    XCTAssertNotNil(weather.currentTemperature, @"There must be a color when current temperature is available.");
}

- (void)testThereIsNoColorWhenTemparatureNotAvailable {
    Weather* weather=[Weather new];
    XCTAssertNil(weather.currentTemperature, @"There must not be a color when there is no current temperature available.");
}

- (void)testCountryNameInEnglish {
    
    // set up the current locale to en_US
    id localeMock = [OCMockObject mockForClass:[NSLocale class]];
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [[[localeMock stub] andReturn:locale] currentLocale];
    
    Weather* weather=[Weather new];
    weather.countryCode=@"es";
    XCTAssertEqualObjects(weather.countryName, @"Spain", @"The country name ES in English must be Spain.");
    
    weather.countryCode=@"gb";
    XCTAssertEqualObjects(weather.countryName, @"United Kingdom", @"The country name GB in English must be United Kingdom.");
}

- (void)testCountryNameInSpanish {
    
    // set up the current locale to es_ES
    id localeMock = [OCMockObject mockForClass:[NSLocale class]];
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"];
    [[[localeMock stub] andReturn:locale] currentLocale];
    
    Weather* weather=[Weather new];
    weather.countryCode=@"es";
    XCTAssertEqualObjects(weather.countryName, @"España", @"The country name ES in Spanish must be España.");
    
    weather.countryCode=@"gb";
    XCTAssertEqualObjects(weather.countryName, @"Reino Unido", @"The country name GB in Spanish must be Reino Unido.");
}

- (void)testNoCountryNameBecauseNoCountryCode {
    Weather* weather=[Weather new];
    XCTAssertNil(weather.countryName, @"There must be no country name when there is no country code.");
}

@end
