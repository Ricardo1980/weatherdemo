//
//  MainViewControllerTests.m
//  WeatherDemo
//
//  Created by Ricardo Ruiz on 29/8/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MainViewController.h"
#import "WeatherClientAPI.h"
#import "Weather.h"
#import <OCMock/OCMock.h>

@interface MainViewControllerTests : XCTestCase

@end

@implementation MainViewControllerTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

/*
 http://kaspermunck.github.io/2012/10/testing-a-view-controllers-iboutlet-connections/
 */
- (void)testOutlets {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController* nvc = [storyboard instantiateInitialViewController];
    MainViewController* vc=(MainViewController*)nvc.topViewController;
    [vc loadView];
    
    XCTAssertNotNil(vc.mapView, @"Must connect mapView IBOutlet.");
    XCTAssertNotNil(vc.currentWeatherLabel, @"Must connect currentWeatherLabel IBOutlet.");
    XCTAssertNotNil(vc.updateButton, @"Must connect updateButton IBOutlet.");
}

/*
 test that when pressing the update button, weatherInlocation will be called and that will update the weather label
 http://hackazach.net/code/2014/03/03/effective-testing-with-ocmock/
*/
- (void)testPressingUpdateButtonLaunchesNetworkRequestAndUpdatesWeatherLabel {
    
    // set up the current locale to es_ES
    id localeMock = [OCMockObject mockForClass:[NSLocale class]];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"];
    [[[localeMock stub] andReturn:locale] currentLocale];
    
    // load the navigation vc
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController* nvc = [storyboard instantiateInitialViewController];
    MainViewController* vc=(MainViewController*)nvc.topViewController;
    [vc loadView];
    
    // stub the network request (the response arrives through a block)
    id weatherClientAPIMock = [OCMockObject mockForClass:[WeatherClientAPI class]];
    [[[weatherClientAPIMock stub] andDo:^(NSInvocation *invoke) {
        //2. declare a block with same signature
        void (^weatherStubResponse)(Weather* weather, NSError* error);
        
        //3. link argument 3 with with our block callback
        [invoke getArgument:&weatherStubResponse atIndex:3];
        
        //4. invoke block with pre-defined input
        Weather* weather=[Weather new];
        weather.place=@"Comunidad de Madrid";
        weather.countryCode=@"es";
        weather.humidity=@94;
        weather.currentTemperature=@21;
        weather.maxTemperature=@21;
        weather.minTemperature=@19;
        
        weatherStubResponse(weather, nil);
        
    }] weatherInlocation:[OCMArg isNotNil] completion:[OCMArg isNotNil]];
    
    // inject the client api mock to the controller
    vc.weatherClientAPI=weatherClientAPIMock;
    
    // simulate pressing the update button
    [vc.updateButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    // test the currentWeatherLabel string
    NSString* expectedCurrentWeatherString=@"Place: Comunidad de Madrid\nCountry: España\nHumidity: 94 percentage\nCurrent Temp: 21 Celsius\nMax Temp: 21 Celsius\nMin Temp: 19 Celsius";
    XCTAssertEqualObjects(vc.currentWeatherLabel.text, expectedCurrentWeatherString, @"The string in currentWeatherLabel is not what we expected.");
}

/*
 test that when pressing the update button, weatherInlocation will be called returning and error and that will update the weather label
 */
- (void)testPressingUpdateButtonLaunchesWrongNetworkRequestAndUpdatesWeatherLabel {
    
    // set up the current locale to es_ES
    id localeMock = [OCMockObject mockForClass:[NSLocale class]];
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"];
    [[[localeMock stub] andReturn:locale] currentLocale];
    
    // load the navigation vc
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController* nvc = [storyboard instantiateInitialViewController];
    MainViewController* vc=(MainViewController*)nvc.topViewController;
    [vc loadView];
    
    // stub the network request (the response arrives through a block)
    id weatherClientAPIMock = [OCMockObject mockForClass:[WeatherClientAPI class]];
    [[[weatherClientAPIMock stub] andDo:^(NSInvocation *invoke) {
        //2. declare a block with same signature
        void (^weatherStubResponse)(Weather* weather, NSError* error);
        
        //3. link argument 3 with with our block callback
        [invoke getArgument:&weatherStubResponse atIndex:3];
        NSError* error=[NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Simulated error."}];

        weatherStubResponse(nil, error);
        
    }] weatherInlocation:[OCMArg isNotNil] completion:[OCMArg isNotNil]];
    
    // inject the client api mock to the controller
    vc.weatherClientAPI=weatherClientAPIMock;
    
    // simulate pressing the update button
    [vc.updateButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    // test the currentWeatherLabel string
    NSString* expectedCurrentWeatherString=@"Error retrieving weather.";
    XCTAssertEqualObjects(vc.currentWeatherLabel.text, expectedCurrentWeatherString, @"The string in currentWeatherLabel is not what we expected.");
}

@end
